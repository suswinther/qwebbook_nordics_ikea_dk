<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
${scheduleBean.init(param)}
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Qmatic QWebbook</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles/jquery-ui.css">
    <link rel="stylesheet" href="styles/main.css">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <script src="scripts/jquery.min.js"></script>
    <script src="scripts/jquery-ui.min.js"></script>
    <script src="scripts/main.js?1"></script>
    <script src="scripts/moment-with-locales.js"></script>
    <script src="scripts/jquery-qrcode-0.14.0.min.js"></script>
</head>
<body>

<div id="applicationTitle">
    <!--${scheduleBean.getMessage('common.applicationTitle')}       -->
</div>
<div class="page-wrapper">
    <div id="pageTitle">
    </div>
    <div id="pageDescription">
    </div>
    <div id="errorMessage">
    </div>

    <div id="pageProblem" class="page">
    </div>

    <!-- <div id="pageTooLateToReschedule" class="page">
    </div>

        <div id="pageTooLateToDelete" class="page">
    </div>-->

    <!--First page, find a booking or create a new one-->
    <div id="pageBookOrFind" class="page">
        <table>
            <tr>
                <td>${scheduleBean.getMessage('pageBookOrFind.message')}</td>
            </tr>
            <tr>
                <td>
                    <div id="bookOrFindBook" class="button primary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageBookOrFind.book')}</a>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="bookOrFindFind" class="button secondary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageBookOrFind.find')}</a>
                    </div>

                </td>
            </tr>
        </table>
    </div>

    <!--Create a booking page 1, displays branch, service, date and time-->
    <div id="pageScheduleTime" class="page">
        <div class="input-fields-wrapper">
            <div class="input-field-row branch-service">
                <div id="firstSelectControl"></div>
                <div id="secondSelectControl"></div>
            </div>
            <div class="input-field-row date">
                <%-- <div class="date-title"><span>${scheduleBean.getMessage('field.date')}</span></div> --%>
                <div id="dateControl"></div>
                <div class="input-field-row time">
                    <select id="timeControl"></select>
                </div>
            </div>

        </div>
        <br>
        <table>
            <tr>
                <td>
                    <div id="scheduleTimeBack" class="button secondary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageScheduleTime.back')}</a>
                    </div>
                </td>
                <td>
                    <div id="scheduleTimeOk" class="button primary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageScheduleTime.ok')}</a>
                    </div>
                </td>
            </tr>
            <tr height="20px"></tr>
        </table>
    </div>
    <!--Create new booking page 2, personal details input-->
    <div id="pageScheduleDetails" class="page">
        <div class="input-field-row">
            <span>${scheduleBean.getMessage('field.service')}</span>
            <div class="select-option selected" id="scheduleInfoService"></div>
        </div>
        <div class="input-field-row">
            <span>${scheduleBean.getMessage('field.branch')}</span>
            <div class="select-option selected" id="scheduleInfoBranch"></div>
        </div>
        <div class="input-field-row">
            <span>${scheduleBean.getMessage('field.date')}</span>
            <div class="select-option selected" id="scheduleInfoDate"></div>
        </div>
        <div class="input-field-row">
            <span>${scheduleBean.getMessage('field.time')}</span>
            <div class="select-option selected" id="scheduleInfoTime"></div>
        </div>
        <br>
        <div id="FirstNameRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.firstName')}*</span>
            <input type="text" id="firstNameControl" size="40" class="select-option">
        </div>
        <div id="LastNameRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.lastName')}*</span>
            <input type="text" id="lastNameControl" size="40" class="select-option">
        </div>
        <div  id="DOBRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.dateOfBirth')}*</span>
            <input type="text" id="dateOfBirthControl" size="40" class="select-option date" readonly="readonly">
        </div>
        <div id="EmailRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.email')}*</span>
            <input type="text" id="emailControl" size="40" class="select-option">
        </div>
        <div id="ConfirmEmailRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.confirm.email')}*</span>
            <input type="text" id="confirmEmailControl" size="40" class="select-option">
        </div>
        <div id="PhoneRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.phone')}*</span>
            <input type="text" id="phoneControl" size="40" class="select-option">
        </div>
        <div id="ConfirmPhoneRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.confirm.phone')}*</span>
            <input type="text" id="confirmPhoneControl" size="40" class="select-option">
        </div>
        <div id="NotesRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.notes')}*</span>
            <textarea maxlength="500" rows="5" id="notesControl" class="select-option"></textarea>
        </div>
        <div id="notificationTypeRow" class="input-radio-buttons">
            <span>${scheduleBean.getMessage('field.notification')}*</span>
            <input type="radio" name="notificationTypeGroup" value="notificationTypeSMS"
                   size="40">${scheduleBean.getMessage('field.notification.sms')}
            <input type="radio" name="notificationTypeGroup" value="notificationTypeEmail"
                   size="40">${scheduleBean.getMessage('field.notification.email')}
        </div>
        <div id="book-warning" class="text-warning">
                        <input type="checkbox" name="termsAndCond[]" value="" size="40">
                       ${scheduleBean.getMessage('pageScheduleDetails.warning')}</div>
        <table>
            <tr id="captchaRow">

            </tr>
            <tr>
                <td>
                    <div id="scheduleDetailsBack" class="button secondary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageScheduleTime.back')}</a>
                    </div>
                </td>
                <td>
                    <div id="scheduleDetailsOk" class="button primary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageScheduleDetails.ok')}</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!--Create new booking page 3, confirmation-->
    <div id="pageScheduleResult" class="page">
        <div class="alert alert-info">
            ${scheduleBean.getMessage('pageScheduleResult.messageAlert')}
        </div>
        <p>
            ${scheduleBean.getMessage('pageScheduleResult.message1')}
            <br><br>
            ${scheduleBean.getMessage('pageScheduleResult.message2')}
            <br><br>
        </p>
        <div class="input-field-row">
            <span>${scheduleBean.getMessage('field.customer')}</span>
            <div class="select-option selected" id="scheduleResultCustomer"></div>
        </div>
        <div class="input-field-row">
            <span>${scheduleBean.getMessage('field.branch')}</span>
            <div class="select-option selected" id="scheduleResultBranch"></div>
        </div>
        <div class="input-field-row">
            <span>${scheduleBean.getMessage('field.service')}</span>
            <div class="select-option selected" id="scheduleResultService"></div>
        </div>
        <div class="input-field-row">
            <span>${scheduleBean.getMessage('field.date')}</span>
            <div class="select-option selected" id="scheduleResultDate"></div>
        </div>
        <div class="input-field-row">
            <span>${scheduleBean.getMessage('field.time')}</span>
            <div class="select-option selected" id="scheduleResultTime"></div>
        </div>
        <div class="input-field-row">
            <span>${scheduleBean.getMessage('field.notes')}</span>
            <div class="select-option selected" id="scheduleResultNotes"></div>
        </div>
        <div class="input-field-row">
            <span>${scheduleBean.getMessage('field.bookingRef')}</span>
            <div class="select-option selected" id="scheduleResultBookingRef"></div>
        </div>
        <table id="scheduleResultTable">
            <tr>
                <td id="scheduleResultQRCode"></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="button secondary" id="scheduleResultPrint">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageScheduleResult.print')} </a>
                    </div>
                    <div class="button primary" id="scheduleResultFinish">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageScheduleResult.done')} </a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!--Find existing booking page 1, personal details input search-->
    <div id="pageSearch" class="page">
        <div id="FirstNameSearchRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.firstName')}*</span>
            <input type="text" id="firstNameSearchControl" size="40" class="select-option">
        </div>
        <div id="LastNameSearchRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.lastName')}*</span>
            <input type="text" id="lastNameSearchControl" size="40" class="select-option">
        </div>
        <div  id="DOBSearchRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.dateOfBirth')}*</span>
            <input type="text" id="dateOfBirthSearchControl" size="40" class="select-option" readonly="readonly">
        </div>
        <div id="EmailSearchRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.email')}*</span>
            <input type="text" id="emailSearchControl" size="40" class="select-option">
        </div>
        <div id="PhoneSearchRow" class="input-field-row select">
            <span>${scheduleBean.getMessage('field.phone')}*</span>
            <input type="text" id="phoneSearchControl" size="40" class="select-option">
        </div>

        <table>
            <tr>
                <td>
                    <div id="searchDetailsBack" class="button secondary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageSearch.back')}</a>
                    </div>
                </td>
                <td>
                    <div id="searchDetailsOk" class="button primary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageSearch.ok')}</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!--Find existing booking page 2, found appointments-->
    <div id="pageFound" class="page">
        <table id="foundApp">
            <thead>
            <tr>
                <th class="control-label">${scheduleBean.getMessage('field.select')}</th>
                <th class="control-label">${scheduleBean.getMessage('field.date')}</th>
                <th class="control-label">${scheduleBean.getMessage('field.time')}</th>
                <th class="control-label">${scheduleBean.getMessage('field.branch')}</th>
                <th class="control-label">${scheduleBean.getMessage('field.service')}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <table class="reschedule-buttons">
            <tr>
                <td>
                    <div id="foundBack" class="button secondary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageFound.back')}</a>
                    </div>
                </td>
                <td>
                    <div id="foundCancel" class="button">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageFound.cancel')}</a>
                    </div>
                </td>
                <td>
                    <div id="foundReschedule" class="button primary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageFound.reschedule')}</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div id="pageChange" class="page">
      <div class="input-field-row">
          <span>${scheduleBean.getMessage('field.customer')}</span>
          <div class="select-option selected" id="changeCustomer"></div>
      </div>
      <div class="input-field-row">
          <span>${scheduleBean.getMessage('field.service')}</span>
          <div class="select-option selected" id="changeService"></div>
      </div>
      <div class="input-field-row">
          <span>${scheduleBean.getMessage('field.branch')}</span>
          <div class="select-option selected" id="changeBranch"></div>
      </div>
      <div class="input-field-row">
          <span>${scheduleBean.getMessage('field.date')}</span>
          <div class="select-option selected" id="changeDate"></div>
      </div>
      <div class="input-field-row">
          <span>${scheduleBean.getMessage('field.time')}</span>
          <div class="select-option selected" id="changeTime"></div>
      </div>
        <table>
            <tr>
                <td>
                    <div id="changeDelete" class="button secondary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageChange.delete')}</a>
                    </div>
                </td>
                <td>
                    <div  id="changeReschedule" class="button primary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageChange.reschedule')}</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div id="pageNotFound" class="page">
    </div>
    <div id="pageSearchNotFound" class="page">
        <div class="button primary">
            <a id="pageSearchNotFoundBack" href="javascript:void(0);">${scheduleBean.getMessage('pageSearchNotFound.back')}</a>
        </div>
    </div>
    <div id="pageError" class="page">
        <table>
            <tr>
                <td>
                    <div class="button primary">
                        <a id="errorBack" href="javascript:void(0);">${scheduleBean.getMessage('pageSearchNotFound.back')}</a>
                    </div>
                </td>

            </tr>
        </table>
    </div>
    <div id="pageAlreadyAppointment" class="page">
        <table>

            <tr>
                <td>
                    <div class="button primary">
                       <!--<a id="alreadyAppointmentBack" href="javascript:void(0);">${scheduleBean.getMessage('pageAlreadyAppointment.back')}</a>-->
                    </div>
                </td>

            </tr>
        </table>
    </div>
    <!--Find existing booking page, delete appointment page -->
    <div id="pageDelete" class="page">
        <table>
            <tr>
                <td>
                    <div id="deleteCancel" class="button secondary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageDelete.cancel')}</a>
                    </div>
                </td>
                <td>
                    <div id="deleteOk" class="button primary">
                        <a href="javascript:void(0);">${scheduleBean.getMessage('pageDelete.ok')}</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!--Find existing booking page, after deletion of an appointment page -->
    <div id="pageDeleteResult" class="page">
        <div class="button primary">
            <a id="deleteResultBack" href="javascript:void(0);">${scheduleBean.getMessage('pageDeleteResult.back')}</a>
        </div>
    </div>
    <div id="processingMessage">
      <div class="loader"></div>
    </div>


    <script>

        $().ready(function () {
            //console.log('Messages', ${scheduleBean.messages});
            //console.log('Service', ${scheduleBean.serviceList});
            var messages = ${scheduleBean.messages};
            var properties = ${scheduleBean.properties};
            var branchList = ${scheduleBean.branchList}["branch"];
            var serviceList = ${scheduleBean.serviceList}["service"];
            var appointment = ${scheduleBean.appointment};
            appModel.setMessages(messages);
            appModel.setProperties(properties);

            switch (${scheduleBean.mode}) {
                case 0:
                    appModel.initProblem();
                    break;
                case 1:
                    appModel.initSchedule(branchList, serviceList);
                    break;
                case 2:
                    appModel.initChange(branchList, serviceList, appointment);
                    break;
                case 3:
                    appModel.initNotFound();
                    break;
            }
        });

        function enableBookButton() {
            $("#book-warning").show();
            $("#captcha").hide();
            $("#scheduleDetailsOk").toggleClass('disabled', false);
            $("#scheduleDetailsOk").off("click");
            $("#scheduleDetailsOk").on("click", appModel.onScheduleDetailsOk);
        }


    </script>
</div>
</body>
</html>
