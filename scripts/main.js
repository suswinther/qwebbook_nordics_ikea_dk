var appModel = (function () {

  var messages;
  var properties;
  var changeMode = false;
  var changeAppointment = null;
  var foundAppointments = [];
  var appointmentId;
  var branchId = '';
  var serviceId = '';
  var date = '';
  var time = '';

  var $errorMessage;
  var $pageTitle;
  var $pageDescription;

  var $branchControl;
  var $serviceControl;
  var $dateControl;
  var $timeControl;

  var $firstNameControl;
  var $lastNameControl;
  var $dateOfBirthControl;
  var $emailControl;
  var $confirmEmailControl;
  var $phoneControl;
  var $confirmPhoneControl;
  var $notesControl;

  var $firstNameSearchControl;
  var $lastNameSearchControl;
  var $dateOfBirthSearchControl;
  var $emailSearchControl;
  var $phoneSearchControl;


  var $serviceFirst;
  var $captchaEnabled;
  var $captchaSiteKey;
  var $captchaSecretKey;
  var $languageCode = "en";
  var $emailRegex;
  var $phoneRegex;
  var $timeFormat = "HH:MM";
  var $showSingleService = true;
  var $showSingleBranch = true;
  var $addInfo = false;
  var $rescheduleTimeEnabled = false;
  var $rescheduleMinutes = -1;
  var $cancelTimeEnabled = false;
  var $cancelMinutes = -1;
  var $uiFields;
  var $showDOB = true;
  var $showQR = false;
  var $showNotes = true;
  var $showNotificationType = false;
  var $showConfirmEmail = false;
  var $showFindPage = false;
  var $nrSelServices;
  var $nrSelBranches;
  var $branchInfo1 = [];
  var $branchInfo3 = [];
  var $branchInfo1Control;
  var $minBookingDay = 0;

  var $branchList;
  var $serviceList;

  var $selectedServiceId = null;
  var $selectedBranchId = null;

  function init() {
    initRTL();
    $errorMessage = $("#errorMessage");
    $processingMessage = $("#processingMessage");
    $pageTitle = $("#pageTitle");
    $pageDescription = $("#pageDescription");
    $languageCode = window.navigator.userLanguage || window.navigator.language;

    if ($serviceFirst) {
      $branchControl = $("#secondSelectControl");
      $serviceControl = $("#firstSelectControl");
      $branchInfo1Control = $("#secondSelectBranchInfo");
      if ($branchInfo1[0].length == 0)
        $("#secondSelectBranchLabel").hide();
      $("#firstSelectBranchLabel").hide();
      $("#firstSelectLabel").html(messages["field.service"]);
      $("#secondSelectLabel").html(messages["field.branch"]);
    } else {
      $branchControl = $("#firstSelectControl");
      $serviceControl = $("#secondSelectControl");
      $serviceControl.hide();
      $branchInfo1Control = $("#firstSelectBranchInfo");
      if ($branchInfo1[0].length == 0)
        $("#firstSelectBranchLabel").hide();
      $("#secondSelectBranchLabel").hide();

      $("#firstSelectLabel").html(messages["field.branch"]);
      $("#secondSelectLabel").html(messages["field.service"]);
    }
    $dateControl = $("#dateControl");
    $dateControl.hide();
    $timeControl = $("#timeControl");
    $timeControl.hide();

    $firstNameControl = $("#firstNameControl");
    $lastNameControl = $("#lastNameControl");
    $dateOfBirthControl = $("#dateOfBirthControl");
    $emailControl = $("#emailControl");
    $confirmEmailControl = $("#confirmEmailControl");
    $phoneControl = $("#phoneControl");
    $confirmPhoneControl = $("#confirmPhoneControl");
    $notesControl = $("#notesControl");
    $termsAndCond = $("input[name='termsAndCond[]']");

    $firstNameSearchControl = $("#firstNameSearchControl");
    $lastNameSearchControl = $("#lastNameSearchControl");
    $dateOfBirthSearchControl = $("#dateOfBirthSearchControl");
    $emailSearchControl = $("#emailSearchControl");
    $phoneSearchControl = $("#phoneSearchControl");

    $branchControl.append($('<label>', {
      text: $("<div />").html(messages["pageScheduleTime.branchOption"]).text(),
      addClass: "title"

    }));

    $serviceControl.append($("<label>", {
      text: $("<div />").html(messages["pageScheduleTime.serviceOption"]).text(),
      addClass: "title"
    }));

    if (getParameterByName("lang") != null)
      $languageCode = getParameterByName("lang");

    appModel.initCaptcha();

    url = "scripts/datepicker-" + $languageCode + ".js";
    var scriptTag = document.createElement('script');
    scriptTag.src = url;
    document.body.appendChild(scriptTag);

    $.datepicker.setDefaults($.datepicker.regional[$languageCode]);
    langMoment = $languageCode.toLowerCase();
    moment.locale(langMoment);

    $.datepicker.setDefaults({
      firstDay: 1
    });

    var today = new Date();
    $dateControl.datepicker({
      //minDate: 0,
      minDate: today.toLocaleDateString($languageCode),
      maxDate: -1
    });

    $timeControl.append($("<option/>", {
      value: '',

      text: $("<div />").html(messages["pageScheduleTime.timeOption"]).text()
    }));

    $dateOfBirthControl.datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '-100y:c+nn',
      maxDate: '-1d'
    });
    $dateOfBirthSearchControl.datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '-100y:c+nn',
      maxDate: '-1d'
    });
    $branchControl.on("change", appModel.onBranchChange);
    $serviceControl.on("change", appModel.onServiceChange);
    $dateControl.on("change", appModel.onDateChange);
    $timeControl.on("change", appModel.onTimeChange);
    $("#scheduleTimeOk").on("click", appModel.onScheduleTimeOk);
    if ($showFindPage) {
      $("#scheduleTimeBack").on("click", appModel.onScheduleTimeBack);
    } else
      $("#scheduleTimeBack").hide();
    $("#bookOrFindBook").on("click", appModel.onBookOrFindBook);
    $("#bookOrFindFind").on("click", appModel.onBookOrFindFind);
    $("#searchDetailsBack").on("click", appModel.returnToStartPage);
    $("#searchDetailsOk").on("click", appModel.onSearchDetailsOk);
    $("#foundBack").on("click", appModel.returnToSearchPage);
    $("#foundReschedule").on("click", appModel.onFoundReschedule);
    $("#foundCancel").on("click", appModel.onFoundCancel);
    $("#pageSearchNotFoundBack").on("click", appModel.returnToSearchPage);
    $("#scheduleDetailsBack").on("click", appModel.onScheduleDetailsBack);
    $("#scheduleResultPrint").on("click", appModel.print);
    $("#scheduleResultFinish").on("click", appModel.returnToStartPage);
    $("#changeDelete").on("click", appModel.onChangeDelete);
    $("#changeReschedule").on("click", appModel.onChangeReschedule);
    $("#deleteCancel").on("click", appModel.onDeleteCancel);
    $("#deleteOk").on("click", appModel.onDeleteOk);
    $("#deleteResultBack").on("click", appModel.returnToStartPage);
    //$("#alreadyAppointmentBack").on("click", appModel.returnToScheduleTimePage);
    $("#errorBack").on("click", appModel.returnToStartPage);

    appModel.hideShowUIFields();
    if ($nrSelServices == 1 && !$showSingleService) {
      $serviceControl.hide();
      if ($serviceFirst) {
        $("#firstSelectLabel").hide();
      } else {
        $("#secondSelectLabel").hide();
      }
    }
    if ($nrSelBranches == 1 && !$showSingleBranch) {
      $branchControl.hide();
      if (!$serviceFirst) {
        $("#firstSelectLabel").hide();
      } else {
        $("#secondSelectLabel").hide();
      }
    }

    $.ajaxSetup({
      cache: false
    });

    $(document).ajaxError(function (event, xhr, options, exc) {
      var msg = xhr.responseText;
      if (msg == null || msg == '')
        msg = "Unexpected ajax error, message '" + exc + "'";
      showError(msg);
    });
  }

  function initRTL() {
    //check for RTL rendering
    try {
      if (messages['direction'] == "rtl") {
        var styles_rtl_file = document.createElement("link");
        styles_rtl_file.setAttribute("href", "styles/bootstrap.rtl.css");
        styles_rtl_file.setAttribute("type", "text/css");
        styles_rtl_file.setAttribute("rel", "stylesheet");
        if (typeof styles_rtl_file != "undefined") {
          document.getElementsByTagName("head")[0].appendChild(styles_rtl_file);
        }
      }
    } catch (e) {
      //nothing found; rtl prop not set
    }
  }

  function listClear(control) {
    control.find('option:not(:first)').remove().val('');
  }

  function listPopulate(control, list, value, text) {
    for (var i = 0; i < list.length; i++) {
      control.append($("<option/>", {
        value: list[i][value],
        text: list[i][text]
      }));
    }
    showHideDropdowns(control, list, list[0][value]);


  }

  function listTimeFormatPopulate(control, list, value, text) {
    for (var i = 0; i < list.length; i++) {
      var formattedTime = moment(list[i][text], "hhmm").format($timeFormat);
      control.append($("<option/>", {
        value: list[i][value],
        text: formattedTime
      }));
    }
  }

  function listPopulateFromCustom(control, list, value, field, branch) {
    if (!branch) {
      control.find("select").remove();
      control.append($("<select></select>"));
      control.show();
      control.find("#serviceDescription").remove();
      control.append($("<p id=\"serviceDescription\"></p>"));
    } else {
      control.append($('<form action=""></form>'));
    }
    control.find("input").remove();
    for (var i = 0; i < list.length; i++) {
      var langName = "";
      customObjectString = list[i][field];
      if (customObjectString != null) {
        try {
          customObj = JSON.parse(customObjectString);
          if (customObj != null) {
            languageNames = customObj['names'];
            serviceDescription = customObj['serviceDescription'];
            if (languageNames != null)
              langName = languageNames[$languageCode];
          }
        } catch (e) {
          // do not do anything, just use the standard names in this case
        }

      }
      else {
        serviceDescription = "";
      }
      if (typeof langName == 'undefined' || langName.length == 0) {
        langName = list[i]['name'];
      }
      if (branch) {
        control.find('form').append('<input type="radio" ' + 'value="' + list[i][value] + '" name="' + langName + '">' + langName + '<span class="branch-address"> - ' + list[i].addressLine1 + ', ' + list[i].addressZip + ', ' + list[i].addressCity + '</span><br>')
      } else {
        var ser = control.find('select').append($("<option/>", {
          value: list[i][value],
          text: langName,
          data: {
            description: serviceDescription
          }
        }));
        ser.change(function() {
          var $selected = $(this).find(':selected');
          $('#serviceDescription').html($selected.data('description'));
        });
        showHideDropdowns(control, list, list[0][value]);
      }

    }
  }

  function showHideDropdowns(control, list, value) {
    if (list.length == 1) {
      if (control == $serviceControl) {
        if (!$showSingleService) {
          $serviceControl.hide();
          if ($serviceFirst) {
            $("#firstSelectLabel").hide();
          } else {
            $("#secondSelectLabel").hide();
          }
          $serviceControl.val(value);
          appModel.onServiceChange();
        }
      } else if (control == $branchControl) {
        if (!$showSingleBranch) {
          $branchControl.hide();
          if (!$serviceFirst) {
            $("#firstSelectLabel").hide();
          } else {
            $("#secondSelectLabel").hide();

          }
          $branchControl.val(value);
          appModel.onBranchChange();
        }
      }
    }
    else {
      $serviceControl.find('select').val($selectedServiceId);
    }
  }

  function navigate(page) {

    resetError();

    $pageTitle.html(messages[page + ".title"]);
    description = messages[page + ".description"];
    if (typeof description == "undefined")
      description = "";
    $pageDescription.html(description);

    $('.page').css('display', 'none');
    $('#' + page).css('display', 'block');
  }

  function navigateError(page) {

    resetError();

    $pageTitle.html(messages[page + ".title"]);
    description = messages[page + ".description"];
    if (typeof description == "undefined")
      description = "";
    $pageDescription.html(description);

    $('.page').css('display', 'none');
    $('#pageError').css('display', 'block');
  }

  function showProcessing(message) {
    $processingMessage.html(message);
    $processingMessage.show();
  }

  function resetProcessing() {
    $processingMessage.hide();
  }

  function showError(message) {
    $errorMessage.html(message);
    $errorMessage.show();
    setTimeout(resetError, 2000);
  }

  function resetError() {
    $errorMessage.hide();
  }

  function onScheduleDetailsOkHelper() {
    response = $("#g-recaptcha-response").val();
    showProcessing(messages["pageScheduleDetails.processing"]);
    $('#scheduleDetailsBack').hide();
    $('#scheduleDetailsOk').hide();

    if (changeAppointment == null) {

      if ($dateOfBirthControl.datepicker("getDate") == null)
        formattedBirthDate = '';
      else
        formattedBirthDate = moment($dateOfBirthControl.datepicker("getDate")).format("YYYY-MM-DD");
      notificationTypeSMS = $("input[name=notificationTypeGroup][value=notificationTypeSMS]")[0].checked;
      notificationTypeEmail = $("input[name=notificationTypeGroup][value=notificationTypeEmail]")[0].checked;
      notificationType = "";
      if (notificationTypeSMS)
        notificationType = "sms";
      if (notificationTypeEmail)
        notificationType = "email";
      $.ajax({
        type: "POST",
        contentType: 'application/json',
        url: '/qwebbook/rest/schedule/appointments/' + appointmentId + '/confirm',
        dataType: "json",
        data: JSON.stringify({
          "customer": {
            "firstName": $firstNameControl.val().encodeHtml(),
            "lastName": $lastNameControl.val().encodeHtml(),
            "dateOfBirth": formattedBirthDate,
            "email": $emailControl.val().encodeHtml(),
            "phone": $phoneControl.val().encodeHtml()
          },
          "notes": $notesControl.val().encodeHtml(),
          "captcha": response,
          "languageCode": $languageCode,
          "notificationType": notificationType
        }),
        success: function (appointment) {
          if (appointment.errorMessage.length > 0) {
            if (appointment.errorMessage == "ERROR_TOO_MANY_RESCHEDULES_AND_TOO_EARLY") {
              navigateError('pageTooManyReschedulesAndTooEarly')
            } else if (appointment.errorMessage == "ERROR_TOO_MANY_RESCHEDULES")
              navigateError('pageTooManyReschedules')
            else
              navigateError('pageTooManyAppointments');
            resetProcessing();
            $('#scheduleDetailsBack').show();
            $('#scheduleDetailsOk').show();
          } else {
            $("#scheduleResultCustomer").html($firstNameControl.val().encodeHtml() + ' ' + $lastNameControl.val().encodeHtml());
            $("#scheduleResultService").html($serviceControl.find('option:selected').text());
            branchInfo = appModel.fillBranchInfoPage3($branchControl.find("input:checked").index('input')); //($branchControl.children("option").filter(":selected").text());
            $("#scheduleResultBranch").html(branchInfo);
            if ($showNotes) {
              $("#scheduleResultNotes").html($notesControl.val().encodeHtml());
            } else {
              $("#scheduleResultNotes").hide();
            }
            var formattedDate = moment(date, "YYYY-MM-DD").format('L');
            var formattedTime = moment(time, "hh:mm").format($timeFormat);
            $("#scheduleResultDate").html(formattedDate);
            $("#scheduleResultTime").html(formattedTime);
            $("#scheduleResultBookingRef").html(appointment.externalId);
            if ($showQR) {
              var options = {
                render: 'image',
                text: appointment.externalId,
                size: 100
              }
              $("#scheduleResultQRCode").empty();
              $("#scheduleResultQRCode").qrcode(options);
            }
            if ($addInfo && appointment.additionalInfo) {
              $("#addInfo").html(appointment.additionalInfo);
            }

            navigate('pageScheduleResult');
            $('#scheduleDetailsBack').show();
            $('#scheduleDetailsOk').show();
            resetProcessing();
            $pageDescription.html($firstNameControl.val().encodeHtml() + ' ' + $lastNameControl.val().encodeHtml() + ',');
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          if (jqXHR.status == 403) {
            resetProcessing();
            $('#scheduleDetailsBack').show();
            $('#scheduleDetailsOk').show();
            $("#book-warning").hide();
            $("#captcha").show();
            showError(messages["pageScheduleDetails.captchaInvalid"]);
          }
        }

      });
    } else {
      $("#scheduleResultCustomer").html($firstNameControl.val().encodeHtml() + ' ' + $lastNameControl.val().encodeHtml());
      $("#scheduleResultService").html($serviceControl.children("option").filter(":selected").text());
      branchInfo = appModel.fillBranchInfoPage3($branchControl.find("input:checked").index('input')); //($branchControl.children("option").filter(":selected").text());
      $("#scheduleResultBranch").html(branchInfo);
      $("#scheduleResultNotes").html($notesControl.val().encodeHtml());
      var formattedDate = moment(date, "YYYY-MM-DD").format('L');
      var formattedTime = moment(time, "hh:mm").format($timeFormat);
      $("#scheduleResultDate").html(formattedDate);
      $("#scheduleResultTime").html(formattedTime);
      $("#scheduleResultBookingRef").html(changeAppointment.externalId);
      if ($addInfo && changeAppointment.additionalInfo) {
        $("#addInfo").html(changeAppointment.additionalInfo);
      }

      navigate('pageScheduleResult');
      $('#scheduleDetailsBack').show();
      $('#scheduleDetailsOk').show();
      resetProcessing();
      $pageDescription.html($firstNameControl.val().encodeHtml() + ' ' + $lastNameControl.val().encodeHtml() + ',');
    }

  }

  function isValidEmail(email) {
    if (email.length === 0) {
      return false;
    }
    var pattern = new RegExp($emailRegex);
    return pattern.test(email);
  };

  function isValidPhoneNumber(number) {
    if (number.length === 0)
      return true;
    var pattern = new RegExp($phoneRegex);
    return pattern.test(number);
  }

  function isValidDOB(dateOfBirth) {
    if (dateOfBirth < 8)
      return false;
    birthDay = new Date(dateOfBirth);
    if (isNaN(birthDay.getTime()))
      return false;
    else
      return true;
  }

  function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
  }

  var enabledDays = [];

  function enableAll(date) {
    var m = date.getMonth() + 1,
      n = "",
      d = date.getDate(),
      e = "",
      y = date.getFullYear();
    if (m < 10) {
      n = "0";
    }
    if (d < 10) {
      e = "0";
    }
    for (i = 0; i < enabledDays.length; i++) {
      if ($.inArray(y + '-' + n + m + '-' + e + d, enabledDays) != -1) {
        return [true]; // If you want to disabled this date just return false from here and return true end of the function instead of return false;
      }
    }
    return [false];
  }

  return {

    setMessages: function (m) {
      messages = m;
    },

    setProperties: function (p) {
      properties = p;
      $captchaEnabled = properties["captchaEnabled"] == "true" ? true : false;
      $captchaSiteKey = properties["captchaSiteKey"];
      $captchaSecretKey = properties["captchaSecretKey"];
      $emailRegex = JSON.parse(properties.regexEmail);
      $phoneRegex = JSON.parse(properties.regexPhone);
      $addInfo = properties["useCustomAdd"] == "true" ? true : false;
      branchInfo1 = properties["branchInfo1"];
      branchInfo1 = properties["branchInfo1"];
      $branchInfo1 = branchInfo1.split(",");
      branchInfo3 = properties["branchInfo3"];
      $branchInfo3 = branchInfo3.split(",");
      timeFormat = properties["timeFormat"];
      if (timeFormat.length > 0)
        $timeFormat = timeFormat;
      $showSingleService = properties["showSingleService"] == "true" ? true : false;
      $showSingleBranch = properties["showSingleBranch"] == "true" ? true : false;
      $rescheduleTimeEnabled = properties["rescheduleTimeEnabled"] == "true" ? true : false;
      $rescheduleMinutes = properties["rescheduleTimeBefore"];
      $cancelTimeEnabled = properties["cancelTimeEnabled"] == "true" ? true : false;
      $cancelMinutes = properties["cancelTimeBefore"];
      $uiFields = JSON.parse(properties["uiFields"]);
      $showQR = properties["showQR"] == "true" ? true : false;
      $showFindPage = properties["showFind"] == "true" ? true : false;
      $showPrintButton = properties["showPrint"] == "true" ? true : false;

      if (!$showPrintButton)
        $("#scheduleResultPrint").hide();
      $showNotificationType = properties["showNotificationType"] == "true" ? true : false;
      if (!$showNotificationType)
        $("#notificationTypeRow").hide();

      $nrSelServices = properties["nrSelServices"];
      $nrSelBranches = properties["nrSelBranches"];
      $serviceFirst = properties["serviceFirst"] == "true" ? true : false;

    },


    initProblem: function () {
      init();
      navigate('pageProblem');
    },

    initSchedule: function (branchList, serviceList) {
      if (branchList != undefined)
        $branchList = branchList;
      if (serviceList != undefined)
        $serviceList = serviceList;
      init();
      if ($serviceFirst) {
        listPopulateFromCustom($serviceControl, $serviceList, 'publicId', 'custom', false);
      } else {
        listPopulateFromCustom($branchControl, $branchList, 'id', 'custom', true);
      }


      if ($showFindPage) {
        navigate('pageBookOrFind');
      } else {
        navigate('pageScheduleTime');
      }
    },

    initChange: function (branchList, serviceList, appointment) {
      console.log('serviceList: ', serviceList);
      console.log('branchList: ', branchList);
      changeMode = true;
      if (branchList != undefined)
        $branchList = branchList;
      if (serviceList != undefined)
        $serviceList = serviceList;
      init();


      changeAppointment = appointment;
      console.log('changeAppointment: ', changeAppointment);
      if (changeAppointment.customer) {
        $firstNameControl.val(changeAppointment.customer.firstName);
        $lastNameControl.val(changeAppointment.customer.lastName);
        $dateOfBirthControl.val(changeAppointment.customer.dateOfBirth);
        $emailControl.val(changeAppointment.customer.email);
        $phoneControl.val(changeAppointment.customer.phone);
      } else {
        $firstNameControl.val(changeAppointment['firstName']);
        $lastNameControl.val(changeAppointment['lastName']);
        $dateOfBirthControl.val(changeAppointment['dateOfBirth']);
        $emailControl.val(changeAppointment['email']);
        $phoneControl.val(changeAppointment['phone']);
      }

      if (changeAppointment.publicBranchId) {
        branchId = changeAppointment.publicBranchId;
      } else {
        branchId = changeAppointment.branchId;
      }
      if (changeAppointment.services && changeAppointment.services.length > 0) {
        serviceId = changeAppointment.services[0].publicId;
      } else {
        serviceId = changeAppointment.serviceId;
      }
      listPopulateFromCustom($branchControl, branchList, 'id', 'custom');
      $branchControl.find('option[value=' + branchId + ']').attr('selected', true);
      listPopulateFromCustom($serviceControl, serviceList, 'publicId', 'custom', false);
      $serviceControl.find('option[value=' + serviceId + ']').attr('selected', true);

      $("#changeCustomer").html($firstNameControl.val().encodeHtml() + ' ' + $lastNameControl.val().encodeHtml());
      $("#changeService").html($serviceControl.find('option:selected').text());
      var branchText = appModel.fillBranchInfoPage3($branchControl.find("option:selected").index('option'));
      $("#changeBranch").html(branchText);
      $("#changeDate").html(moment(appointment['date'], 'YYYY-MM-DD').format('L'));
      $("#changeTime").html(appointment['time']);

      navigate('pageChange');
      appModel.onServiceChange();
    },

    initNotFound: function () {
      init();
      navigate('pageNotFound');
    },

    onBranchChange: function () {
      if (branchId && !$selectedBranchId) {
        $('input[value=' + branchId + ']').attr('checked', false);
      }
      branchId = $branchControl.find('input:checked').val();
      serviceId = $serviceControl.val();
      //appModel.fillBranchInfoPage1($branchControl.prop('selectedIndex'));
      if ($serviceFirst) {
        $dateControl.datepicker("destroy").datepicker({
          minDate: $minBookingDay,
          maxDate: -1,
        }).datepicker("refresh");
        appModel.onDateChange();

        if (serviceId != '') {
          $.ajax({
            contentType: 'application/json',
            url: '/qwebbook/rest/schedule/branches/' + branchId + '/services/' + serviceId + '/dates',
            dataType: "json",
            data: '',
            success: function (dateList) {
              enabledDays = [];
              for (var i = 0; i < dateList.length; i++) {
                enabledDays.push(dateList[i]['date']);
              }

              if (dateList.length > 0) {
                today = moment(0, 'HH').add($minBookingDay, 'days');
                for (i = 0; i <= $minBookingDay; i++) {
                  start = moment(dateList[i]['date'], "YYYY-MM-DD");
                  if (start.isSameOrAfter(today)) {
                    $minDate = new Date(dateList[i]['date']);
                    break;
                  }
                }

                $maxDate = new Date(dateList[dateList.length - 1]['date']);
                $("#dateControl").datepicker("destroy").datepicker({
                  dateFormat: "YYYY-MM-DD",
                  minDate: $minDate,
                  maxDate: $maxDate,
                  defaultDate: $minDate,
                  beforeShowDay: enableAll
                }).datepicker("refresh");
                appModel.onDateChange();
              }
            }
          });
        }
      } else {
        listClear($serviceControl);
        if (branchId != '') {
          $.ajax({
            contentType: 'application/json',
            url: '/qwebbook/rest/schedule/branches/' + branchId + '/services',
            dataType: "json",
            data: '',
            success: function (data) {
              listPopulateFromCustom($serviceControl, data, 'publicId', 'custom', false);
              if ($selectedServiceId != null) {
                $serviceControl.val($selectedServiceId);
                appModel.onServiceChange();
              }
            }
          });
        }
      }
    },

    onServiceChange: function () {
      if (!branchId) {
        branchId = $branchControl.val();
      }

      serviceId = $serviceControl.find('select option:selected').val();
      if ($serviceFirst) {
        listClear($branchControl);

        if (serviceId != '') {
          $.ajax({
            contentType: 'application/json',
            url: '/qwebbook/rest/schedule/services/' + serviceId + '/branches',
            dataType: "json",
            data: '',
            success: function (data) {
              console.log(data);
              $branchList = data;
              listPopulateFromCustom($branchControl, data, 'id', 'custom', true);
              if ($selectedBranchId != null) {
                $branchControl.val($selectedBranchId);
                appModel.onBranchChange();
              }
            }
          });
        }

      } else {
        $dateControl.datepicker("destroy").datepicker({
          minDate: 0,
          maxDate: -1,
        }).datepicker("refresh");
        appModel.onDateChange();
        if (serviceId != '' && serviceId !== undefined) {
          $.ajax({
            contentType: 'application/json',
            url: '/qwebbook/rest/schedule/branches/' + branchId + '/services/' + serviceId + '/dates',
            dataType: "json",
            data: '',
            success: function (dateList) {
              enabledDays = [];
              for (var i = 0; i < dateList.length; i++) {
                enabledDays.push(dateList[i]['date']);
              }

              if (dateList.length > 0) {
                today = moment(0, "HH").add($minBookingDay, 'days');
                for (i = 0; i <= $minBookingDay; i++) {
                  start = moment(dateList[i]['date'], "YYYY-MM-DD");
                  if (start.isSameOrAfter(today)) {
                    $minDate = new Date(dateList[i]['date']);
                    break;
                  }
                }

                $maxDate = new Date(dateList[dateList.length - 1]['date']);
                $("#dateControl").datepicker("destroy").datepicker({
                  dateFormat: "YYYY-MM-DD",
                  minDate: $minDate,
                  maxDate: $maxDate,
                  defaultDate: $minDate,
                  beforeShowDay: enableAll
                }).datepicker("refresh");
                $dateControl.show();
                appModel.onDateChange();
              }
            }
          });
        }
      }
    },

    onDateChange: function () {
      $timeControl.show();

      if ($dateControl.datepicker("getDate") == null || $.datepicker.formatDate("yy-mm-dd", $dateControl.datepicker("getDate")) < $.datepicker.formatDate("yy-mm-dd", new Date()))
        date = '';
      else {
        var thisDate = moment($dateControl.datepicker("getDate"));
        month = (thisDate.month() + 1) < 10 ? "0" + (thisDate.month() + 1) : (thisDate.month() + 1);
        day = thisDate.date() < 10 ? "0" + thisDate.date() : thisDate.date();
        date = thisDate.year() + "-" + month + "-" + day;
      }

      listClear($timeControl);
      appModel.onTimeChange();

      if (date != '') {

        $.ajax({
          contentType: 'application/json',
          url: '/qwebbook/rest/schedule/branches/' + branchId + '/services/' + serviceId + '/dates/' + date + '/times',
          dataType: "json",
          data: '',
          success: function (data) {
            listTimeFormatPopulate($timeControl, data, 'time', 'time');
          }
        });
      }
    },

    onTimeChange: function () {
      time = $timeControl.val();
    },


    onBookOrFindFind: function () {
      navigate('pageSearch');
    },
    onBookOrFindBook: function () {
      navigate('pageScheduleTime');
    },

    onScheduleTimeOk: function () {
      if (branchId == '' || serviceId == '' || date == '' || time == '') {
        showError(messages["pageScheduleTime.errorRequired"]);
        return;
      }

      if (changeAppointment == null) {
        $.ajax({
          type: "POST",
          contentType: 'application/json',
          url: '/qwebbook/rest/schedule/branches/' + branchId + '/services/' + serviceId + '/dates/' + date + '/times/' + time + '/reserve',
          dataType: "json",
          data: '{"appointment" : {"customers":[], "resources":[]}}',
          success: function (appointment) {
            appointmentId = appointment['publicId'];
            navigate('pageScheduleDetails');
          }
        });
      } else {
        if (changeAppointment.customer) {
          $firstNameControl.val(changeAppointment.customer.firstName);
          $lastNameControl.val(changeAppointment.customer.lastName);
          $dateOfBirthControl.val(moment(changeAppointment.customer.dateOfBirth, 'YYYY-MM-DD').format('L'));
          $emailControl.val(changeAppointment.customer.email);
          $confirmEmailControl.val(changeAppointment.customer.email);
          $phoneControl.val(changeAppointment.customer.phone);
          $confirmPhoneControl.val(changeAppointment.customer.phone);
          $notesControl.val(changeAppointment.notes);
          navigate('pageScheduleDetails');
        } else {
          $firstNameControl.val(changeAppointment.firstName);
          $lastNameControl.val(changeAppointment.lastName);
          $dateOfBirthControl.val(moment(changeAppointment.dateOfBirth, 'YYYY-MM-DD').format('L'));
          $emailControl.val(changeAppointment.email);
          $confirmEmailControl.val(changeAppointment.email);
          $phoneControl.val(changeAppointment.phone);
          $confirmPhoneControl.val(changeAppointment.phone);
          $notesControl.val(changeAppointment.notes);
          navigate('pageScheduleDetails');
        }
      }


      $("#scheduleInfoService").html($serviceControl.find('option:selected').text());
      var branchText = appModel.fillBranchInfoPage3($branchControl.find("input:checked").index('input'));
      $("#scheduleInfoBranch").html(branchText);


      var formattedDate = moment(date, "YYYY-MM-DD").format("DD/MM/YYYY");
      $("#scheduleInfoDate").html(formattedDate);
      var formattedTime = moment(time, "hh:mm").format($timeFormat);
      $("#scheduleInfoTime").html(formattedTime);
      if ($captchaEnabled) {
        $("#book-warning").hide();
        $("#captcha").show();
      } else {
        $("#book-warning").show();
        $("#captcha").hide();
        $("#scheduleDetailsOk").toggleClass('disabled', false);
        $("#scheduleDetailsOk").off("click");
        $("#scheduleDetailsOk").on("click", appModel.onScheduleDetailsOk);
      }

    },

    onScheduleDetailsBack: function () {
      // There is no way to cancel a confirmed slot so just leave it and let Orchestra delete it
      appModel.onDateChange();
      navigate('pageScheduleTime');
    },


    onScheduleDetailsOk: function () {
      response = $("#g-recaptcha-response").val();


      if (!appModel.checkMandatoryFieldsFilled()) {
        showError(messages["pageScheduleDetails.errorRequired"]);
        return;
      }
      if (!isValidEmail($emailControl.val().encodeHtml())) {
        showError(messages["pageScheduleDetails.errorEmail"]);
        return;
      }

      if (!isValidPhoneNumber($phoneControl.val().encodeHtml())) {
        showError(messages["pageScheduleDetails.errorPhone"]);
        return;
      }

      if (!$termsAndCond.is(':checked')) {
        showError(messages["pageScheduleDetails.errorConsent"]);
        return;
      }
      /*if (!isValidDOB($dateOfBirthControl.val())){
          showError(messages["pageScheduleDetails.invalidDateOfBirth"]);
          return;
      }*/


      if (!appModel.checkConfirmField()) {
        return;
      }

      if (changeAppointment != null) {
        if ($dateOfBirthControl.datepicker("getDate") == null)
          formattedBirthDate = '';
        else
          formattedBirthDate = moment($dateOfBirthControl.datepicker("getDate")).format("YYYY-MM-DD");
        $.ajax({
          type: "POST",
          url: '/qwebbook/rest/schedule/appointments/' + changeAppointment['appointmentId'] + "/reschedule",
          dataType: "json",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          data: JSON.stringify({
            "notes": $notesControl.val().encodeHtml(),
            "captcha": response,
            "languageCode": $languageCode,
            "date": date,
            "time": time,
            "publicBranchId": $branchControl.val(),
            "publicServiceId": $serviceControl.val(),
            "customer": {
              "firstName": $firstNameControl.val(),
              "lastName": $lastNameControl.val(),
              "dateOfBirth": formattedBirthDate,
              "email": $emailControl.val(),
              "phone": $phoneControl.val()
            }

          }),
          success: function (data) {
            changeAppointment = data;
            if (changeAppointment.duration == 0) {
              navigateError('pageTooManyReschedules');
              resetProcessing();
              $('#scheduleDetailsBack').show();
              $('#scheduleDetailsOk').show();
            } else {
              onScheduleDetailsOkHelper();
            }
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            showError(messages["pageTooLateToReschedule.description"]);
          }
        });

      } else {

        onScheduleDetailsOkHelper();
      }
    },

    onSearchDetailsOk: function () {
      response = $("#g-recaptcha-response").val();

      if (!appModel.checkSearchFieldsFilled()) {
        showError(messages["pageSearch.errorRequired"]);
        return;
      }

      $.ajax({
        type: "POST",
        url: '/qwebbook/rest/schedule/appointments/search',
        dataType: "json",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({
          "captcha": response,
          "firstName": $firstNameSearchControl.val(),
          "lastName": $lastNameSearchControl.val(),
          "email": $emailSearchControl.val(),
          "phone": $phoneSearchControl.val(),
          "dob": $dateOfBirthSearchControl.val(),

        }),
        success: function (data) {
          foundAppointments = data;
          if (foundAppointments.length == 0)
            navigateError("pageSearchNotFound");
          else {
            html = "";
            for (i = 0; i < foundAppointments.length; i++) {
              app = foundAppointments[i];
              id = app["id"];
              html += '<tr>';
              html += '<td><input type="checkbox" name="changeApp" value="' + i + '"></td>';
              var formattedDate = moment(app['date'], "YYYY-MM-DD").format('L');
              var formattedTime = moment(app['time'], "hh:mm").format($timeFormat);
              html += '<td>' + formattedDate + "</td>";
              html += '<td>' + formattedTime + "</td>";
              html += '<td>' + app['branchName'] + "</td>";
              html += '<td>' + app['serviceName'] + "</td>";
              html += '</tr>';
            }
            $("#foundApp tbody").html(html);
            navigate("pageFound");
          }
        }
      });
    },

    onFoundReschedule: function () {
      index = $('input[name=changeApp]:checked').val();
      changeAppointment = foundAppointments[index];
      appointmentId = changeAppointment['publicId'];
      changeAppointment['appointmentId'] = appointmentId;
      appModel.onChangeReschedule();
    },
    onFoundCancel: function () {
      index = $('input[name=changeApp]:checked').val();
      changeAppointment = foundAppointments[index];
      appointmentId = changeAppointment['publicId'];
      changeAppointment['appointmentId'] = appointmentId;
      appModel.onChangeDelete();
    },

    onChangeDelete: function () {
      now = new Date();
      var appointmentDate = new Date();
      var dateString = changeAppointment['date'];
      var timeString = changeAppointment['time'];
      var dateM = moment(dateString);
      var timeM = moment(timeString, $timeFormat);

      var appointmentDate = new Date(dateM.year(), dateM.month(), dateM.date(), timeM.hour(), timeM.minute());

      var diffMS = (appointmentDate.getTime() - now.getTime()) / 1000 / 60;
      var minutes = Math.floor(diffMS);

      if (minutes < 0 || ($cancelTimeEnabled && $cancelMinutes != -1 && minutes < $cancelMinutes)) {
        navigateError('pageTooLateToDelete');
      } else {
        navigate('pageDelete');
      }
    },

    onChangeReschedule: function () {
      var id;
      if (changeAppointment.publicId) {
        id = changeAppointment.publicId;
      } else {
        id = changeAppointment.appointmentId;
      }
      $.ajax({
        type: "GET",
        url: '/qwebbook/rest/schedule/appointments/' + id + '/checkReschedule',
        dataType: "json",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        success: function (data) {
          if (data.rule) {
            rule = data.rule;
          }
          if (data.maxReached) {
            maxReached = data.maxReached;
          }
          if (data.tooLate) {
            tooLate = data.tooLate;
          }
          if (data.ruleDays) {
            $minBookingDay = parseInt(data.ruleDays);
          }

          if (maxReached == "false" || (maxReached == "true" && rule == "ruleFromDay")) {
            console.log('changeAppointment: ', changeAppointment);
            if (changeAppointment.publicBranchId) {
              $selectedBranchId = changeAppointment.publicBranchId;
            } else {
              $selectedBranchId = changeAppointment.branchId;
            }
            if (changeAppointment.services && changeAppointment.services.length > 0) {
              $selectedServiceId = changeAppointment.services[0].publicId;
            } else {
              $selectedServiceId = changeAppointment.serviceId;
            }
            if ($serviceFirst) {
              listPopulateFromCustom($serviceControl, $serviceList, 'publicId', 'custom', false);
              $serviceControl.val($selectedServiceId);
              appModel.onServiceChange();
            } else {
              $branchControl.find('select').remove();
              listPopulateFromCustom($branchControl, $branchList, 'id', 'custom', true);
              $branchControl.find('input[value=' + $selectedBranchId + ']').attr('checked', true);
              appModel.onBranchChange();
            }

            $branchControl.find('input').attr("disabled", true);
            $serviceControl.attr("disabled", true);
            navigate('pageScheduleTime');
          } else if (tooLate == "true") {
            navigateError('pageTooLateToReschedule');
          } else if (maxReached = "true")
            navigateError('pageTooManyReschedules');
        },
        error: function (error) {
          console.log('ERROR', error);
        }
      });
    },

    onDeleteCancel: function () {
      if (changeMode) {
        navigate('pageChange');
      } else {
        navigate('pageFound');
      }

    },


    onDeleteOk: function () {

      $.ajax({
        type: "DELETE",
        url: '/qwebbook/rest/schedule/appointments/' + changeAppointment['appointmentId'],
        dataType: "json",
        data: '',
        success: function (data) {
          navigate('pageDeleteResult');
        }
      });
    },

    checkMaxReschedule: function (customer) {
      customObjectString = customer['custom'];

      checkMax = properties['maxReschedulesEnabled'];
      if (checkMax == "false")
        return true;
      custom = JSON.parse(customObjectString);
      reschedule = custom['reschedule'];
      cancel = custom['cancel'];
    },


    checkMandatoryFieldsFilled: function () {
      mandatory = $uiFields["LastName"]["m"];
      if (mandatory && $lastNameControl.val().encodeHtml() == '')
        return false;
      mandatory = $uiFields["FirstName"]["m"];
      if (mandatory && $firstNameControl.val().encodeHtml() == '')
        return false;
      mandatory = $uiFields["DOB"]["m"];
      if (mandatory && $dateOfBirthControl.val().encodeHtml() == '')
        return false;
      mandatory = $uiFields["Email"]["m"];
      if (mandatory && $emailControl.val().encodeHtml() == '')
        return false;
      mandatory = $uiFields["ConfirmEmail"]["m"];
      if (mandatory && $confirmEmailControl.val().encodeHtml() == '')
        return false;
      mandatory = $uiFields["Phone"]["m"];
      if (mandatory && $phoneControl.val().encodeHtml() == '')
        return false;
      mandatory = $uiFields["ConfirmPhone"]["m"];
      if (mandatory && $confirmPhoneControl.val().encodeHtml() == '')
        return false;
      mandatory = $uiFields["Notes"]["m"];
      if (mandatory && $notesControl.val().encodeHtml() == '')
        return false;
      return true;
    },

    checkSearchFieldsFilled: function () {
      mandatory = $uiFields["LastName"]["f"];
      if (mandatory && $lastNameSearchControl.val().encodeHtml() == '')
        return false;
      mandatory = $uiFields["FirstName"]["f"];
      if (mandatory && $firstNameSearchControl.val().encodeHtml() == '')
        return false;
      mandatory = $uiFields["DOB"]["f"];
      if (mandatory && $dateOfBirthSearchControl.val().encodeHtml() == '')
        return false;
      mandatory = $uiFields["Email"]["f"];
      if (mandatory && $emailSearchControl.val().encodeHtml() == '')
        return false;
      mandatory = $uiFields["Phone"]["f"];
      if (mandatory && $phoneSearchControl.val().encodeHtml() == '')
        return false;
      return true;
    },

    checkConfirmField: function () {
      show = $uiFields["ConfirmEmail"]["s"];
      if (show && $emailControl.val() != $confirmEmailControl.val()) {
        showError(messages["pageScheduleDetails.emailNotMatching"]);
        return false;
      }

      show = $uiFields["ConfirmPhone"]["s"];
      if (show && $phoneControl.val() != $confirmPhoneControl.val()) {
        showError(messages["pageScheduleDetails.phoneNotMatching"]);
        return false;
      }

      return true;

    },

    hideShowUIFields: function () {
      for (var key in $uiFields) {
        if ($uiFields.hasOwnProperty(key)) {
          var fieldObj = $uiFields[key];
          var show = fieldObj["s"];
          if (!show) {
            var rowName = "#" + key + "Row";
            $(rowName).hide();
            if (key === "Notes") {
              $("#rowNotesResult").hide();
              $showNotes = false;
            }
          }
          if (key == "ConfirmEmail") {
            if (show) {
              $showConfirmEmail = true;
            } else {
              $showConfirmEmail = true;
            }
          }

          var search = fieldObj["f"];
          if (!search) {
            var rowName = "#" + key + "SearchRow";
            $(rowName).hide();
          }
        }
      }

    },
    fillBranchInfoPage1: function (index) {
      branch = $branchList[index - 1];
      branchInfo = "";
      if ($branchInfo1.indexOf("AddressLine1") >= 0 && branch.addressLine1 != undefined)
        branchInfo += branch.addressLine1;
      if ($branchInfo1.indexOf("AddressLine2") >= 0 && branch.addressLine2 != undefined)
        branchInfo += "<br>" + branch.addressLine2;
      if ($branchInfo1.indexOf("City") >= 0 && branch.addressCity != undefined)
        branchInfo += "<br>" + branch.addressCity;
      if ($branchInfo1.indexOf("Postcode") >= 0 && branch.addressZip != undefined)
        branchInfo += "<br>" + branch.addressZip;

      $branchInfo1Control.html(branchInfo);
    },

    fillBranchInfoPage3: function (index) {
      //console.log('index: ',$branchInfo3);
      branch = $branchList[index];
      branchInfo = branch.name;
      if ($branchInfo3.indexOf("AddressLine1") >= 0 && branch.addressLine1 != undefined)
        branchInfo += '<br><div class="branch-address">' + branch.addressLine1;
      if ($branchInfo3.indexOf("AddressLine2") >= 0 && branch.addressLine2 != undefined)
        branchInfo += ", " + branch.addressLine2;
      if ($branchInfo3.indexOf("Postcode") >= 0 && branch.addressZip != undefined)
        branchInfo += ", " + branch.addressZip;
      if ($branchInfo3.indexOf("City") >= 0 && branch.addressCity != undefined)
        branchInfo += " " + branch.addressCity + '</div>';

      return branchInfo;
    },

    initCaptcha: function () {
      if ($captchaEnabled) {
        var script = document.createElement('script');
        script.src = 'https://www.google.com/recaptcha/api.js?hl=' + $languageCode;

        document.head.appendChild(script);
        html = '<td colspan="2">';
        html += '<p id="book-warning" class="text-warning" style="white-space: pre-line">' + messages['pageScheduleDetails.warning'] + '</p>';
        html += '<div id="captcha" class="g-recaptcha" data-sitekey="' + $captchaSiteKey + '" data-callback="enableBookButton" style="-moz-transform:scale(0.77); -ms-transform:scale(0.77); -o-transform:scale(0.77); -moz-transform-origin:0; -ms-transform-origin:0; -o-transform-origin:0; -webkit-transform:scale(0.77); transform:scale(0.77); -webkit-transform-origin:0 0; transform-origin:0; filter: progid:DXImageTransform.Microsoft.Matrix(M11=0.77,M12=0,M21=0,M22=0.77,SizingMethod=\"auto expand\"></div></td>';
        $("#captchaRow").html(html);
      }
    },

    print: function () {
      var htmlData;

      htmlData = '<html><head><meta charset="utf-8"><title>' + messages['pagePrint.title'] + "</title>";
      htmlData += '<style>body {margin: 30px;} @page { size: auto;  margin: 0mm; } th {font-weight:bold; text-align:left; vertical-align: top;} td {padding: 0px 5px 5px 0px; margin: 0px;}</style></head><body><div style="margin:10px">';
      htmlData += '<h1>' + messages['pagePrint.header'] + '</h1>';
      htmlData += '<p><span >' + $("#pageDescription").text() + '</span></p>';
      htmlData += '<p><span>' + messages['pageScheduleResult.message1'] + '</span></p>';

      htmlData += '<p><span>' + messages['pageScheduleResult.message2'] + '</span></p>';
      htmlData += '<p><span>' + $("#addInfo").html() + '</span></p>';
      htmlData += '<table><tr><th class="control-label result">' + messages['field.customer'] + '</th>';
      htmlData += '<td>' + $("#scheduleResultCustomer").text() + '</td></tr>';
      htmlData += '<tr><th class="control-label result">' + messages['field.service'] + '</th>';
      htmlData += '<td>' + $("#scheduleResultService").text() + '</td></tr>';
      htmlData += '<tr><th class="control-label result">' + messages['field.branch'] + '</th>';
      htmlData += '<td>' + $("#scheduleResultBranch").html() + '</td></tr>';
      htmlData += '<tr><th class="control-label result">' + messages['field.date'] + '</th>';
      htmlData += '<td>' + $("#scheduleResultDate").text() + '</td></tr>';
      htmlData += '<tr><th class="control-label result">' + messages['field.time'] + '</th>';
      htmlData += '<td>' + $("#scheduleResultTime").text() + '</td></tr>';
      if ($showNotes) {
        htmlData += '<tr><th class="control-label result">' + messages['field.notes'] + '</th>';
        htmlData += '<td>' + $("#scheduleResultNotes").text() + '</td></tr>';
      }
      htmlData += '<tr><th class="control-label result">' + messages['field.bookingRef'] + '</th>';
      htmlData += '<td>' + $("#scheduleResultBookingRef").text() + '</td></tr>';
      htmlData += '<tr height="10px"></tr>';
      htmlData += '<tr><td>' + $("#scheduleResultQRCode").html() + '</td></tr>';
      htmlData += '</table></div></body></html>';

      var WinPrint = window.open('', '', 'left=0,top=0,width=550,height=650,toolbar=0,scrollbars=0,status=0,location=0,menubar=0');
      WinPrint.document.write(htmlData);
      WinPrint.document.close();
      WinPrint.focus();
      WinPrint.print();
      WinPrint.close();
    },

    /*returnToScheduleTimePage : function(){
        navigate('pageScheduleTime');
    },*/
    returnToStartPage: function () {
      //appModel.initSchedule();
      changeAppointment = null;
      $branchControl.removeAttr("disabled");
      $serviceControl.removeAttr("disabled");
      $selectedServiceId = null;
      $selectedBranchId = null;
      $("#firstSelectControl option:first").attr('selected', 'selected');
      $("#secondSelectControl option:first").attr('selected', 'selected');
      if ($branchList.length > 1)
        $branchInfo1Control.html("");
      var today = new Date();
      $dateControl.datepicker({
        //minDate: 0,
        minDate: today.toLocaleDateString($languageCode),
        maxDate: -1
      });
      $("#timeControl option:first").attr('selected', 'selected');
      if ($showFindPage)
        navigate("pageBookOrFind");
      else
        navigate('pageScheduleTime');
    },
    returnToFoundPage: function () {
      navigate("pageFound");
    },

    returnToSearchPage: function () {
      navigate("pageSearch");
    },
    onScheduleTimeBack: function () {
      if (changeAppointment != null && !changeMode) {
        appModel.returnToFoundPage();
      } else if (changeAppointment != null && changeMode) {
        navigate('pageChange');
      } else {
        appModel.returnToStartPage();
        changeAppointment = null;
      }
      // Now reset the changeAppointment to null
      //changeAppointment = null;
    }


  };

})();


String.prototype.encodeHtml = function () {
  var tagsToReplace = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
  };
  return this.replace(/[&<>]/g, function (tag) {
    return tagsToReplace[tag] || tag;
  });
};

String.prototype.decodeHtml = function () {
  var tagsToReplace = {
    '&amp;': '&',
    '&lt;': '<',
    '&gt;': '>'
  };
  return this.replace(/[&<>]/g, function (tag) {
    return tagsToReplace[tag] || tag;
  });
};
