function loadSchedule() {
    document.getElementById('iframe').src = 'index.jsp';
}

function getRequestParameter(name) {
    if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search))
        return decodeURIComponent(name[1]);
}

var appointmentId = getRequestParameter('appointmentId');
if (appointmentId && appointmentId != '') {
    document.getElementById('iframe').src = 'index.jsp?appointmentId=' + appointmentId;
}